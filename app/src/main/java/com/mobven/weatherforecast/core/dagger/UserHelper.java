package com.mobven.weatherforecast.core.dagger;

public class UserHelper {

  // List<WeatherWrapper> weathersList = new ArrayList<>();
    double latitutude = 0.0;
    double longitude = 0.0;

//    public List<WeatherWrapper> getWeathersList() {
//        return weathersList;
//    }
//
//    public void setWeathersList(List<WeatherWrapper> weathersList) {
//        this.weathersList = weathersList;
//    }

    public double getLatitutude() {
        return latitutude;
    }

    public void setLatitutude(double latitutude) {
        this.latitutude = latitutude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
