package com.mobven.weatherforecast.core.constant;


public class ApiConstant {

    public static String SUCCESS = "SUCCESS";
    public static String APPLOGINSTATUS = "APP-LOGIN-SUCCESS";
    public static String USERLOGINSUCCESS = "USER-LOGIN-SUCCESS";
    public static String NO_RESULT = "NO-RESULT";
    public static String FAILED = "FAILED";
    public static String FAIL = "FAIL";
    public static String OTHER_DEVICE_ON_LOGIN = "OTHER_DEVICE_ON_LOGIN";
    public static String USER_NOT_LOGINNED = "USER-NOT-LOGINNED";
    public static String APP_NOTLOGINNED = "APP-NOTLOGINNED";

    public static final String RANGE = "20";
    public static final String VERSION = "1.0";
    public static final String SODEXO = "sodexo";
    public static final String CAPTCHAANSWER = "";
    public static final String CONTENTCATEGORY = "Yorumlar";
    public static final String PARENTCONTENTID = "99";
    public static final String INSERTCONTENT = "insertComment";
    public static final String FIRSTROW = "0";
    public static final String LASTROW = "20";
    public static final String SUBSCRIBERCHANNEL = "mobile";

    //WEBSERVİCE REQUEST COMMAND
    public static String APPLOGIN = "login";
    public static String USERLOGIN = "userLogin";
    public static String NEARBYPLACES = "getNearestPlaceNew";
    public static String ABOUTDETAIL = "getCompanyAboutDetail";
    public static String GETFORUMCOMMENTSTEXT = "getForumCommentsText";
    public static String GETFORUMCOMMENTSPICTURE = "getForumCommentsPicture";
    public static String SPENDINGDETAIL = "spendDetail";
    public static String ADDCARD = "addCardNew";
    public static String GETCARDAVAILABILITY = "getCardAvailabity";
    public static String USERPROFILEUPDATE = "userProfileUpdate";
    public static String GETPASSPOINTS = "getPassPoints";
    public static String GETRESTRAURANTPASSPOINTS = "getRestaurantPassPointsNew";
    public static String SEARCHMERCHANTSCORE = "searchMerchantByScore";
    public static String GETBALANCE = "getBalance";
    public static String USERPROFILEPHOTOUPDATE = "userProfilePhotoUpdate";
    public static String MYPROFILE = "myProfile";
    public static String USERLOGOUT = "userlogout";
    public static String MYFAVORITES = "getMyFavorite";
    public static String INSERTFAVORITE = "myFavorite";
    public static String CONTACTUS = "contactUs";
    public static String ADDCARDBLOCK = "addCardBlock";
    public static String OSINFO = "lastOs";
    public static String GETCAMPAIGN = "getCampaing";
    public static String GETCAMPAIGNCODE = "getCampaingCode";
    public static String GETGOODLIFE = "getGoodLife";
    public static String GETCAMPAIGNID = "getCampaingID";
    public static String GETGOODLIFEID = "getGoodLifeID";
    public static String REGISTER = "registerUser";
    public static String FORGOTPASSWORD = "forgotPassword";
    public static String FORGOTUSERNAME = "forgotUserName";

    public static String GETLOYALTYINFO = "getLoyalytInfo";
    public static String GETLOYALITYSTATUS = "getLoyalityStatus";
    public static String SETLOYALITYREGISTER = "setLoyalityRegister";
    public static String GETPARAMS = "getParams";

    public static String GETLISTCATEGORY = "getListCategory";
    public static String GETLISTCATEGORYNEW = "getListCategoryNew";
    public static String GETLISTCITY = "getListCity";
    public static String GETLISTDISTRICT = "getListDistrict";
    public static String MENULOGINSERT = "menuLogInsert";
    public static String GETVERSION = "getVersion";


}
